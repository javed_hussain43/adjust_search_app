class RepositoryWorker

	include Sidekiq::Worker

  def perform(*args)
  	logger.info "args = #{args[0]}"
  	Cache::Client.new(key: args[0]).write(args[1])
  end
	
	
end