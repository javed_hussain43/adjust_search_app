module Repository
	class Search
		attr_reader :query_list, :cache_clear

		def initialize(query: , cache_clear:)
			@query_list = query.split(" ").map{|word| "topic:#{word}" }.join("+")
			@cache_clear = cache_clear
		end

		def call
			if cache_clear
				results = search_repositories
			else
				logger("Getting from cache") 
				results = fetch_from_cache
				results = search_repositories if results.nil?
			end
			results
		end

		private
		
		def search_repositories
			logger("Clearing existing cache and getting from API for key=#{query_list}")
			delete_from_cache
			results = Github::Client.new(query: query_list).get_respositories
			write_to_cache(results)
			results
		end

		def fetch_from_cache
			redis_cache.get
		end

		def write_to_cache results
			#redis_cache.write(results)
			RepositoryWorker.perform_async(query_list, results)
		end

		def delete_from_cache
			redis_cache.evict
		end

		def redis_cache
			Cache::Client.new(key: query_list)
		end
		
		def logger(message)
			Rails.logger.info("#{message}")
		end
		
	end
end