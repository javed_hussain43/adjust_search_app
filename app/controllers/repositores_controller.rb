class RepositoresController < ApplicationController

  def search
  	if params[:search].present?
  		@respostories = Repository::Search.new(
  			query: params[:search], 
  			cache_clear: params[:cache_clear]
  			).call
  	end
  end

  def show
  end
end
