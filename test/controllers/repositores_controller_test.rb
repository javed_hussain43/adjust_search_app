require 'test_helper'

class RepositoresControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get repositores_home_url
    assert_response :success
  end

  test "should get search" do
    get repositores_search_url
    assert_response :success
  end

  test "should get show" do
    get repositores_show_url
    assert_response :success
  end

end
