module Github
	module Serializer
		class SearchResultSerializer

			def initialize(object)
				@object = parse_json(object)
			end

			def call
				{
					search_count: @object['items'].size,
					search_result_list: serialize_result_objects(@object['items'])	
				}
				
			end

			private

			def parse_json object
				JSON.parse(object)
			end

			def serialize_result_objects item_objects
				results = []
				item_objects.each do |item|
					results.push(Serializer::SearchResultRepositorySerializer.new(item).call)
				end
				results
			end

		end
	end
end