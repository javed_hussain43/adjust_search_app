module Github
	module Serializer
		class SearchResultRepositorySerializer

			def initialize(object)
				@object = object
			end

			def call
				{ 
					name: @object['name'],
					description: @object['description'],
					full_name: @object['full_name'],
					clone_url: @object['clone_url'],
					forks_count: @object['forks_count']
				}
			end
			
			
		end
	end
end