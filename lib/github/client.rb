require 'net/http'
module Github
	class Client

		BASE_URL = 'https://api.github.com/'
		SEARCH_API_URL = 'search/repositories?q='
		HEADER = {'Accept' => 'application/vnd.github.mercy-preview+json'}

		attr_reader :uri


		def initialize(query: )
			@uri = URI( get_url + query )
		end

		def get_respositories
			http         = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			send_response(http.get(uri.request_uri, initheader = HEADER))
		end

		private
		def get_url
			BASE_URL + SEARCH_API_URL
		end

		def send_response(response)
			Serializer::SearchResultSerializer.new(response.body).call
		end
	end
end