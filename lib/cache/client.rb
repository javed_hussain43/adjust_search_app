require "redis"
module Cache
	class Client

		attr_reader :key, :redis
		
		def initialize(key:)
			@key = key
			@redis = Redis.new
		end

		def get
			payload = redis.get(key)
			payload.nil? ? nil : eval(payload)
		end

		def write(payload)
			redis.set(key, payload)
		end

		def evict
			redis.del(key)
		end
		
		
	end
end