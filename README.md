# Github Search Application

Make a simple web app where you can display public repositories from github based on search term provided. It can be a super simple page with input field on the top of it. Results can be shown right under the input.

# The Solution

1. We have written a client for getting gitub search results.

2. Implemented Redis Cache for storing the results

3. Exposed an API to get the results with an query params.

4. List the search results on the page.



# Enhancements

1. Pagination of results

2. We can go for ElasticSearch to store the information.



# How to Build and Run 

1. Please make sure you have installed ruby verison '2.6.5'

2. Please create a separate gemset and run 'Bundle Install'

3. Run 'foreman start' in the terminal.

4. Go to localhost:3000 and start searching.

